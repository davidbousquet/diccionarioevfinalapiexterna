/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna;

import cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.dao.PalabraJpaController;
import cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.entity.Palabra;
import cl.dto.Entry;
import cl.dto.LexicalEntry;
import cl.dto.Result;

import java.util.Date;
import cl.dto.PalabraDTO;
import cl.dto.Result;
import cl.dto.Sens;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author davidbousquet
 */
@Path("diccionario")
public class ApiInternaDiccionario {

    @GET
    @Path("/{palabraabuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("palabraabuscar") String palabraABuscar) {

        Client client = ClientBuilder.newClient();

        String url = "https://od-api.oxforddictionaries.com:443/api/v2/entries/";
        String idioma = "es/";
        String appId = "1ddbafe5";
        String apiKey = "6b1a11dc448206ff73de556d856ae85d";
        
        url+= idioma;
        url+= palabraABuscar;
        
       

        WebTarget myResource = client.target(url);
        PalabraDTO palabraDTO = myResource.request(MediaType.APPLICATION_JSON).header("app_id", appId).header("app_key", apiKey).get(PalabraDTO.class);

        Date fecha = new Date();

        List<Result> results = palabraDTO.getResults();
        List<LexicalEntry> lexicalEntries = null;
        List<Entry> entries = null;
        List<Sens> senses = null;
        Sens sense = null;

        if (results != null && results.size() > 0) {
            lexicalEntries = results.get(0).getLexicalEntries();
        }
        if (lexicalEntries != null && lexicalEntries.size() > 0) {
            entries = lexicalEntries.get(0).getEntries();
        }
        if (entries != null && entries.size() > 0) {
            senses = entries.get(0).getSenses();
        }
        if (senses != null && senses.size() > 0) {
            sense = senses.get(0);
        }
        Palabra palabra = new Palabra();
        if (sense != null) {
            palabra.setFecha(fecha);
            palabra.setPalabra(palabraABuscar);
            palabra.setDefinicion(sense.getDefinitions().get(0));
            
            PalabraJpaController dao = new PalabraJpaController();
            dao.create(palabra);
            return Response.ok(200).entity(palabra).build();
        }
        
        return Response.serverError().build();


    }
}
