/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.controller;

import cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.ApiInternaDiccionario;
import cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.dao.PalabraJpaController;
import cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.entity.Palabra;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author davidbousquet
 */
@WebServlet(name = "ConsultaPalabraController", urlPatterns = {"/ConsultaPalabraController"})
public class ConsultaPalabraController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsultaPalabraController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsultaPalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Client client = ClientBuilder.newClient();
        //recuperamos parametro
        String palabraAbuscar = request.getParameter("palabraABuscar");
        String accion = request.getParameter("accion");

        if (accion.equals("buscar")) {
            //chequeamos si palabra ya existe en bbdd
            Palabra palabra = buscarSiExiste(palabraAbuscar);
            if (palabra != null) {
                //si existe respondemos con info de bbdd
                request.setAttribute("palabra", palabra);
                //todo redirecto to result
            } else {
                //si no existe llamamos api interna y espoeramos respuesta
                WebTarget myResource = client.target("https://diccionario-oxford.herokuapp.com/api/diccionario/" + palabraAbuscar);
                palabra = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<Palabra>() {
                });
                request.setAttribute("palabra", palabra);
            }

            request.getRequestDispatcher("resultado.jsp").forward(request, response);
        }

        if (accion.equals("listar")) {
            PalabraJpaController dao = new PalabraJpaController();
            List<Palabra> listaPalabras = dao.findPalabraEntities();
            if (listaPalabras == null) {
                listaPalabras = new ArrayList<Palabra>();
            }
            request.setAttribute("listaPalabras", listaPalabras);
            request.getRequestDispatcher("listar.jsp").forward(request, response);
        }

    }

    private Palabra buscarSiExiste(String palabraStr) {
        PalabraJpaController dao = new PalabraJpaController();
        List<Palabra> palabrasEnBBDD = dao.findPalabraEntities();
        if (palabrasEnBBDD != null && palabrasEnBBDD.size() > 0) {
            for (Palabra palabraActual : palabrasEnBBDD) {
                if (palabraStr.toUpperCase().equals(palabraActual.getPalabra().toUpperCase())) {
                    return palabraActual;
                }
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
