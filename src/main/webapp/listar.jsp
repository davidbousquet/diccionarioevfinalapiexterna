<%-- 
    Document   : listar
    Created on : May 8, 2021, 10:22:50 PM
    Author     : davidbousquet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.entity.Palabra"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<%
    List<Palabra> listaPalabras = (List<Palabra>) request.getAttribute("listaPalabras");
    Iterator<Palabra> iterador = listaPalabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <title>Palabras buscadas</title>
    </head>
    <body>
        <div class="container">
            <div class="row my-4">
                <div class="col">
                    <h1>Resultado consulta</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <% if (listaPalabras != null && listaPalabras.size() > 0) {
                    %>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Palabra</th>
                                <th>Definición</th>
                                <th>Fecha búsqueda</th>
                            </tr>
                        </thead>

                        <tbody>
                            <% while (iterador.hasNext()) {
                                    Palabra palabra = iterador.next();%>
                            <tr>
                                <td>
                                    <%= palabra.getPalabra()%>
                                </td>
                                <td>
                                    <%= palabra.getDefinicion()%>
                                </td>
                                <td>
                                    <%= palabra.getFecha()%>
                                </td>
                            </tr>
                        </tbody>

                        <% }%>
                        <% }%>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a class="btn btn-warning" href="./">Volver</a>
                </div>
            </div>
        </div>
    </body>
</html>
