<%-- 
    Document   : resultado
    Created on : May 8, 2021, 9:54:32 PM
    Author     : davidbousquet
--%>

<%@page import="cl.ciisa.ic201iecireol.diccionarioevfinalapiexterna.entity.Palabra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabra palabra = (Palabra) request.getAttribute("palabra");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <title>Resultado</title>
    </head>
    <body>
        <div class="container">
            <div class="row my-4">
                <div class="col">
                    <h1>Diccionario</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="lead">
                        <strong><%= palabra.getPalabra()%> : </strong>
                        <%= palabra.getDefinicion()%> <br />
                        Fecha consulta: <%= palabra.getFecha()%> 
                    </div>
                </div>
                    <div class="col-6">
                        <a class="btn btn-warning" href="./">Volver</a>
                    </div>
            </div>
           
        </div>
    </body>
</html>
