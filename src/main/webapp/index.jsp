<%-- 
    Document   : index
    Created on : May 8, 2021, 5:20:41 PM
    Author     : davidbousquet
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <title>Diccionario</title>
    </head>
    <body>
        <div class="container">
            <div class="row my-4">
                <div class="col">
                    <h1>Diccionario</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <form action="ConsultaPalabraController" method="POST">
                            <label for="palabraABuscar" class="form-label">Ingrese palabra a buscar:</label>
                            <input class="form-control" id="palabraABuscar" name="palabraABuscar">
                            <button class="btn btn-primary my-4 mx-2" name="accion" value="buscar">Buscar</button>
                            <button class="btn btn-success" name="accion" value="listar">Ver Historial de Palabras</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
